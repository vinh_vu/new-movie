import datetime
import random

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

post = {
    "title": "Movie title",
    "desc": "long desc ...",
    "video_link": "https://google.com/",
    "actor_name": "some one",
    "genres": "one;two;three",
    "tags": "one;two;three",
    "img_url": "url img"
    # release date: today
}

driver = webdriver.Chrome('/home/sgs/Documents/chromedriver')
login_url = 'http://somesite.local/wp-login.php?redirect_to=http%3A%2F%2Fsomesite.local%2Fwp-admin%2Fpost-new.php%3Fpost_type%3Dmovie&reauth=1'
driver.get(login_url)
driver.find_element_by_id("user_login").clear()
driver.find_element_by_id("user_login").send_keys("admin")
driver.find_element_by_id("user_pass").clear()
driver.find_element_by_id("user_pass").send_keys("1304076525")
driver.find_element_by_id("wp-submit").click()

driver.find_element_by_xpath('//*[@id="post-title-0"]').send_keys(post["title"])
#
driver.find_element_by_xpath('//*[@id="_movie_choice"]')
# Today as release date
driver.find_element_by_xpath('//*[@id="_movie_release_date"]').send_keys(str(datetime.date.today()))
# set randomly as featured
if random.random() < 0.3:
    driver.find_element_by_xpath('//*[@id="_featured"]').click()

#video url
# driver.find_element_by_xpath('//*[@id="general_movie_data"]/div[1]/div[2]').send_keys('abdc')

#publish button
driver.find_element_by_xpath('//*[@id="editor"]/div/div/div[1]/div/div[1]/div/div[2]/button[2]').click()
driver.find_element_by_xpath('//*[@id="editor"]/div/div/div[1]/div/div[2]/div[3]/div/div/div/div[1]/div/button').click()
driver.close()
