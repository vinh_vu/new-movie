from get_driver import WebDriver

g_drive_link = 'https://gdurl.com/'
create_button_xpath = '//*[@id="index-form"]/div[3]/button'
link_xpath = '//*[@id="created"]/article/a[1]'
link_download_xpath = '//*[@id="created"]/article/a[2]'
root_url = 'https://gdurl.com/'
file_url = 'https://drive.google.com/file/d/1TwBZw4ZPLl6rxp0PCFij_Zle7yyFLs89/view?usp=sharing'
driver = WebDriver().get_driver()
driver.get(g_drive_link)
e = driver.find_element_by_id('url').send_keys(file_url)
driver.find_element_by_xpath(create_button_xpath).click()
print driver.find_element_by_xpath(link_xpath).get_attribute('href')
driver.close()
# app.start_upload_to_drive(link)
