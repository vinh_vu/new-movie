from time import sleep
from get_driver import *


class SmartShare:

    # element = driver.find_element_by_xpath('//*[@id="downloads"]/p[2]/a')
    # element = find_element_by_text('a', '720p')

    def __init__(self):
        wait_duration = 15
        web_link = 'https://smartshare.tv/f/08ndwcllep8eexy'

        web_drv = WebDriver()
        driver = web_drv.get_driver()
        driver.get(web_link)
        driver.find_element_by_id('download').click()
        sleep(wait_duration)
        # link = self.get_highest_video(driver)
        element = web_drv.find_element_by_text('a', '720')
        if element:
            link = element.get_attribute('href')
            print 'link: ', link

  # //*[@id="downloads"]/p[1]/a

    @staticmethod
    def get_highest_video(driver):
        r = reversed(range(5))
        for i in r:
            xpath = '//*[@id="downloads"]/p[{}]/a'.format(i)
            try:
                element = driver.find_element_by_xpath(xpath)
                if element:
                    link = element.get_attribute('href')
                    print 'link: ', link
                    return link
            except:
                continue

SmartShare()

